const emptyStaffList = [];
let staffList = [];
var dataJson = localStorage.getItem("staffList");
if (dataJson !== null) {
  arrayStaffList = JSON.parse(dataJson);
  for (var i = 0; i < arrayStaffList.length; i++) {
    var item = arrayStaffList[i];
    var newStaff = new staff(
      item.account,
      item.name,
      item.email,
      item.pass,
      item.day,
      item.baseSalary,
      item.role,
      item.hour
    );
    staffList.push(newStaff);
  }
  renderStaff(staffList);
}

function renderStaff(staffList) {
  var contentHTML = "";
  for (var i = 0; i < staffList.length; i++) {
    var staff = staffList[i];
    var contentTR = `<tr>
    <td>${staff.account}</td>
    <td>${staff.name}</td>
    <td>${staff.email}</td>
    <td>${staff.day}</td>
    <td>${staff.role}</td>
    <td>${staff.sumSalary()}</td>
    <td>${staff.classification()}</td>
    
    <td> <button class = "btn btn-danger"
     onclick="delStaff('${staff.account}')">Xoá</button> 

    <button class = "btn btn-primary" 
    data-toggle="modal" 
    data-target="#myModal" 
    onclick="modifyStaff('${staff.account}')">Sửa</button> </td> 
    </tr>`;
    contentHTML = contentHTML + contentTR;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

document.getElementById("btnThem").addEventListener("click", function () {
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
});

document.getElementById("btnThemNV").addEventListener("click", function () {
  var staff = getData();
  var valid = Validation(staff);

  if (valid == false) {
    return;
  } else {
    staffList.push(staff);
    renderStaff(staffList);
    var staffListJson = JSON.stringify(staffList);
    localStorage.setItem("staffList", staffListJson);
  }
});

function delStaff(account) {
  var location = getLocation(staffList, account);
  staffList.splice(location, 1);
  renderStaff(staffList);
}

function modifyStaff(account) {
  document.getElementById("btnCapNhat").style.display = "block";
  document.getElementById("btnThemNV").style.display = "none";
  var loc = getLocation(staffList, account);
  showData(staffList[loc]);
  document
    .getElementById("btnCapNhat")
    .addEventListener("click", function (account) {
      var staffUpdated = getData();
      var valid = Validation(staffUpdated);
      if (valid == false) {
        return;
      } else {
        staffList[loc] = staffUpdated;
        renderStaff(staffList);
      }
    });
}

// click vào searh button sau khi nhập loại nhân viên để tìm kiếm
document.getElementById("btnTimNV").addEventListener("click", function () {
  var typeOfStaff = document.getElementById("searchName").value;
  var staffRole = [];
  staffRole = staffList.filter(function (item) {
    return item.role === "Nhân Viên";
  });

  renderStaff(emptyStaffList);
  var a = staffType(staffRole, typeOfStaff);
  renderStaff(a);
});

// nhập loại nhân viên vào ô xong bấm Enter để tìm kiếm
document
  .getElementById("searchName")
  .addEventListener("keypress", function (e) {
    if (e.key === `Enter`) {
      var typeOfStaff = document.getElementById("searchName").value;
      var staffRole = [];
      staffRole = staffList.filter(function (item) {
        return item.role === "Nhân Viên";
      });

      renderStaff(emptyStaffList);
      var a = staffType(staffRole, typeOfStaff);
      renderStaff(a);
    }
  });
