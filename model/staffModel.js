var staff = function (
    _account,
    _name,
    _email,
    _pass,
    _day,
    _baseSalary,
    _role,
    _hour
  ) {
    this.account = _account;
    this.name = _name;
    this.email = _email;
    this.pass = _pass;
    this.day = _day;
    this.baseSalary = _baseSalary;
    this.role = _role;
    this.hour = _hour;
    this.sumSalary = function () {
      if (this.role === "Sếp") {
        return this.baseSalary * 3;
      }
      if (this.role === "Trưởng phòng") {
        return this.baseSalary * 2;
      }
      if (this.role === "Nhân Viên") {
        return this.baseSalary;
      }
    };
    this.classification = function () {
      if (this.role === "Nhân Viên") {
        if (this.hour >= 192) {
          return "Nhân viên xuất sắc";
        }
        if (this.hour < 192 && this.hour >= 176) {
          return "Nhân viên giỏi";
        }
        if (this.hour < 176 && this.hour >= 160) {
          return "Nhân viên khá";
        }
        if (this.hour < 160) {
          return "Nhân viên trung bình";
        }
      } else return "Không xếp loại";
    };
  };
  