function Validation(staff) {
  var account = staff.account;
  var name = staff.name;
  var email = staff.email;
  var pass = staff.pass;
  var baseSalary = staff.baseSalary;
  var hour = staff.hour;
  var day = staff.day;

  // valid1 = emptyTest(account, `tbTKNV`, "Mã số nhân viên"); validation thừa nếu thích thì mở lên vẫn dc
  valid1 = emptyTest(name, `tbTen`, "Tên nhân viên");
  valid2 = emptyTest(email, `tbEmail`, "Email");
  // valid4 = emptyTest(pass, `tbMatKhau`, "Mật khẩu"); validation thừa nếu thích thì mở lên vẫn dc
  valid3 = emptyTest(baseSalary, `tbLuongCB`, "Lương cơ bản");
  valid4 = emptyTest(hour, `tbGiolam`, "Giờ làm");
  valid5 = roleTest();
  valid6 = letterTest(name, `tbTen1`, "Tên nhân viên");
  valid7 = sameValueTest(account, `tbTKNV1`, "Mã số nhân viên");
  valid8 = emptyTest(day, `tbNgay`, "Ngày làm");
  valid9 = minMaxLetterTest(account, 4, 6, `tbTKNV2`, "Mã số nhân viên");
  valid10 = minMaxLetterTest(pass, 6, 10, `tbMatKhau`, "Mật khẩu");
  valid11 = emailTest(email, `tbEmail1`);
  valid12 = passLetterTest(pass, `tbMatKhau1`);
  valid13 = minMaxValueTest(baseSalary, `tbLuongCB1`, 1e6, 20e6);
  valid14 = minMaxValueTest(hour, `tbGiolam1`, 80, 200);

  if (
    valid1 == true &&
    valid2 == true &&
    valid3 == true &&
    valid4 == true &&
    valid5 == true &&
    valid6 == true &&
    valid7 == true &&
    valid8 == true &&
    valid9 == true &&
    valid10 == true &&
    valid11 == true &&
    valid12 == true &&
    valid13 == true &&
    valid14 == true
  ) {
    return true;
  } else {
    return false;
  }
}
