function getLocation(staffList, accountStaff) {
  var location = staffList.findIndex(function (item) {
    return item.account == accountStaff;
  });
  return location;
}

var getData = function () {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var pass = document.getElementById("password").value;
  var day = document.getElementById("datepicker").value;
  var baseSalary = document.getElementById("luongCB").value;
  var role = document.getElementById("chucvu").value;
  var hour = document.getElementById("gioLam").value;

  var newStaff = new staff(
    account,
    name,
    email,
    pass,
    day,
    baseSalary,
    role,
    hour
  );

  return newStaff;
};

var showData = function (staff) {
  document.getElementById("tknv").value = staff.account;
  document.getElementById("name").value = staff.name;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.pass;
  document.getElementById("datepicker").value = staff.day;
  document.getElementById("luongCB").value = staff.baseSalary;
  document.getElementById("chucvu").value = staff.role;
  document.getElementById("gioLam").value = staff.hour;
};

function staffType(staffList, value) {
  if (value === "trung bình") {
    var a = staffList.filter(function (item) {
      return item.hour >= 80 && item.hour < 160;
    });
  }
  if (value === "khá") {
    var a = staffList.filter(function (item) {
      return item.hour >= 160 && item.hour < 176;
    });
  }
  if (value === "giỏi") {
    var a = staffList.filter(function (item) {
      return item.hour >= 176 && item.hour < 192;
    });
  }
  if (value === "xuất sắc") {
    var a = staffList.filter(function (item) {
      return item.hour >= 192 && item.hour <= 200;
    });
  }
  return a;
}
