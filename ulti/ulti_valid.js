function emptyTest(value, selectorID, name) {
  if (value === "") {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(selectorID).innerText =
      name + " " + `không được bỏ trống`;
    return false;
  } else {
    document.getElementById(selectorID).style.display = "none";
    return true;
  }
}

function letterTest(value, selectorID, name) {
  var regexLetter = /^[A-Z a-z]+$/;
  // kiểm tra kí tự từ a-z A-Z và khoảng trắng là hợp lệ
  if (regexLetter.test(value)) {
    document.getElementById(selectorID).style.display = "none";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(selectorID).innerText =
      name + " " + `phải là chữ cái 100%`;
    return false;
  }
}

function roleTest() {
  var a = document.getElementById("chucvu").value;
  if (a == 0) {
    document.getElementById("tbChucVu").style.display = "block";
    document.getElementById(
      "tbChucVu"
    ).innerText = `Chức vụ không được để trống`;
    return false;
  } else {
    document.getElementById("tbChucVu").style.display = "none";
    return true;
  }
}

function sameValueTest(value, selectorID, name) {
  var loc = getLocation(staffList, value);
  if (loc == -1) {
    document.getElementById(selectorID).style.display = "none";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(selectorID).innerText =
      name + " " + `đã có người đăng ký trước`;
    return false;
  }
}

function minMaxValueTest(value, selectorID, min, max) {
  if (value >= min && value <= max) {
    document.getElementById(selectorID).style.display = "none";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(
      selectorID
    ).innerText = `Giá trị từ ${min} đến ${max}`;
    return false;
  }
}

function minMaxLetterTest(value, min, max, selectorID, name) {
  var a = value.length;
  if (a >= min && a <= max) {
    document.getElementById(selectorID).innerText = "";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(selectorID).innerText =
      name + " " + `dài từ ${min} đến ${max} kí tự`;
    return false;
  }
}

function emailTest(value, selectorID) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(value)) {
    document.getElementById(selectorID).style.display = "none";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(
      selectorID
    ).innerText = `Email không đúng định dạng`;
    return false;
  }
}

function passLetterTest(value, selectorID) {
  var regularExpression =
    /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  if (regularExpression.test(value)) {
    document.getElementById(selectorID).style.display = "none";
    return true;
  } else {
    document.getElementById(selectorID).style.display = "block";
    document.getElementById(
      selectorID
    ).innerText = `Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`;
    return false;
  }
}
